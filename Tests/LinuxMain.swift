import XCTest
@testable import FluffyTests

XCTMain([
    testCase(FluffyTests.allTests),
])
